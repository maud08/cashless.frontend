import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserFormCreateComponent } from './features/user/user-form-create/user-form-create.component';
import { UserListComponent } from './features/user/user-list/user-list.component';
import { IsLoggedGuard } from 'src/app/core/guards/is-logged.guard';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch:'full'},
  { path: 'core', loadChildren: () => import('./core/core.module').then(m => m.CoreModule) },
  { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) },
  { path: 'login', loadChildren: () => import('./features/user/user.module').then(m => m.UserModule)},
  { path:'inscription', component: UserFormCreateComponent},
  { path: 'user', component:UserListComponent, canActivate: [IsLoggedGuard]},
  { path: '**', redirectTo: 'home', pathMatch: 'full'}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
