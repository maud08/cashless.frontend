export interface UserModel{
    firstName: string;
    lastName: string;
    address: string;
    email: string;
    password: string;
}