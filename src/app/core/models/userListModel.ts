export interface UserListModel{
    id: Number;
    first_name: string;
    last_name: string;
    address: string;
    email: string;
    group: [];
}