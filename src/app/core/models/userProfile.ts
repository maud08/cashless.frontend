export interface UserProfile{
    firstName?: string;
    lastName?: string;
    address?: string;
    email?: string;
    password?: string;
}