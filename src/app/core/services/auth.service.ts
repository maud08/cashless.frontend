import { Injectable } from '@angular/core';
import { LoginModel } from '../models/loginModel';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TokenModel } from '../models/tokenModel';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpClient: HttpClient
  ) { }
  
  //récupération du token et connexion 
  login(login: LoginModel): Observable<TokenModel>{
    return this.httpClient.post<TokenModel>(environment.apiDomaine + '/login_check', login)
  }
}
