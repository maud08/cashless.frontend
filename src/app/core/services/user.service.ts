import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserModel } from '../models/userModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { finalize, retry } from 'rxjs/operators';
import { UserListModel } from '../models/userListModel';
import { SessionService } from './session.service';
import { UserProfile } from '../models/userProfile';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _context$: BehaviorSubject<UserModel[]>;

  public get context$(): Observable<UserModel[]> {
    return this._context$.asObservable();
  };

  constructor(
    private httpClient: HttpClient,
    private session: SessionService
  ) {
    this._context$ = new BehaviorSubject<UserModel[]>([]);
  }

  refresh():void{
    this.httpClient
      .get<UserModel[]>(environment.apiDomaine + '/user')
      .subscribe(data => this._context$.next(data));
  }

  insert(user: UserModel): Observable<UserModel>{
    return this.httpClient
    .post<UserModel>(environment.apiDomaine + '/inscription', user)
    .pipe(finalize(() => this.refresh()));
  }
  
  public getAll(): Observable<UserListModel[]>{
    return this.httpClient
      .get<UserListModel[]>(
        environment.apiDomaine + '/user',
        {headers: new HttpHeaders({'Authorization': 'Bearer' + this.session.token})}
      );
  }

  public getProfile(): Observable<UserListModel[]>{
    return this.httpClient
      .get<UserListModel[]>(
        environment.apiDomaine+ '/profile',
        {headers: new HttpHeaders({'Authorization': 'Bearer' + this.session.token})}
      );
  }

  public delete(id: number): Observable<UserListModel>{
    return this.httpClient
      .delete<UserListModel>(
        environment.apiDomaine +'/user/'+ id,
        {headers: new HttpHeaders({'Authorization': 'Bearer' + this.session.token})}
      );
  }

  public changeGroup(id: number):Observable<UserListModel>{
    return this.httpClient
      .put<UserListModel>(
        environment.apiDomaine +'/user/'+ id, {label: 'GROUP_ADMIN'},
        {headers: new HttpHeaders({'Authorization': 'Bearer' + this.session.token})}
      );
  }

  public updateProfile(id: number, user: UserListModel[]):Observable<{token: string, user: UserListModel}>{
    return this.httpClient
      .put<{token: string, user: UserListModel}>(
        environment.apiDomaine +'/profile/'+ id, user,
        {headers: new HttpHeaders({'Authorization': 'Bearer' + this.session.token})}
      )
  }

}
