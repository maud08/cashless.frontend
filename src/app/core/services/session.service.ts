import { Injectable } from '@angular/core';
import { TokenModel } from '../models/tokenModel';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  //Enregistrement du token dans le local storage
  start(token: TokenModel){
    sessionStorage.setItem('TOKEN', token.token);
  }

  //Verifie si on est logger ( si le token est là)
  get islogged() :boolean{
    return sessionStorage.getItem('TOKEN') != null;
  }

  //récuper le token a remplacer par un behaviorsubject
  get token(): string{
    return sessionStorage.getItem('TOKEN');
  }
}
