import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private session: SessionService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(this.session.token != null)
    {
      let clone = request.clone({setHeaders: {'Authorization': 'Bearer ' + this.session.token}})
      return next.handle(clone);
    }
    
    return next.handle(request);
  }
}
