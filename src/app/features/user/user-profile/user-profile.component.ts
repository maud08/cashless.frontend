import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { UserListModel } from 'src/app/core/models/userListModel';
import { FormGroup, FormControlName, Validators, FormControl } from '@angular/forms';
import { UserProfile } from 'src/app/core/models/userProfile';
import { UserModel } from 'src/app/core/models/userModel';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  model: UserListModel[] = [];
  editMode = false;
  editMode1 = false;
  editMode2 = false;
  userUpdate: FormGroup;


  error_msg = {
    'name': [
      { type: 'required', message:'Votre prénom et nom sont requis'},
      { type: 'max', message:'Votre nom est trop long'}
    ],
    'address': [
      { type: 'required', message:'Votre adresse est requise'}
    ],
    'email':[
      { type: 'required', message:'Votre Email est requis'},
      { type: 'email', message:'Votre Email est incorrect'}
    ],
    'password': [
      { type: 'required', message:'Votre mot de passe est requis'},
      { type: 'pattern', message:'Votre mot passe doit contenir minimun 8 caratères, une majuscule, une minuscule, un nombre et un caratère spécial'},
      { type: 'max', message: 'Votre mot passe doit contenir au maximun 20 caractères'}
    ]
  }

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.getProfile()
      .subscribe(
        data => {
          this.model = data; 
          this.userUpdate.patchValue(this.model);
        },
        error => console.log 
      )
    this.userUpdate = new FormGroup({
      'lastName': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.max(255)
      ])),
      'firstName': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.max(255)
      ])),
      'address': new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'email': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.email
      ])),
      'password' : new FormControl(null, Validators.compose([
        Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}'),
        Validators.max(20)
      ]))
    });
  }
  updateInput(event: KeyboardEvent, id: number){
 //permet de valider lors de l'enter
    if (event.key == 'Enter') {
      
      this.userService.updateProfile(id, this.userUpdate.value)
        .subscribe(data=> {
          console.log(data);
          if(data){
            const index = this.model.findIndex(m => m.id == id);
            this.model[index] = data["user"];
            this.editMode = false;
            this.editMode1 = false;
            this.editMode2 = false;
            sessionStorage.setItem('TOKEN', data.token);
          }

        }, error => {
          console.log("erreur",error);
        })
    }
    
  }

}
