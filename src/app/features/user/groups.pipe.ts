//permet d'aller rechercher le label dans le tableau json
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'groups'
})
export class GroupsPipe implements PipeTransform {

  transform(groups: any[], separator: string = ','): string {
    const labels = groups.map(g => g.label);
    return labels.join(separator);
  }


}
