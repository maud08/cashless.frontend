import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserFormCreateComponent } from './user-form-create/user-form-create.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { GroupsPipe } from './groups.pipe';


@NgModule({
  declarations: [UserComponent, UserLoginComponent, UserFormCreateComponent, UserListComponent, UserProfileComponent, GroupsPipe],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule
  ]
})
export class UserModule { }
