import { Component, OnInit } from '@angular/core';
import { UserListModel } from 'src/app/core/models/userListModel';
import { UserService } from 'src/app/core/services/user.service';
import { FormGroup} from '@angular/forms';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  model: UserListModel[]= [];
  userActionForm: FormGroup;

  constructor(
    private userService: UserService

  ) { }

  ngOnInit(): void {
    this.userService.getAll()
      .subscribe(
        data => this.model = data,
        error => console.log
      );
  }

  delete(id: number){
    this.userService.delete(id)
    .subscribe(data => {
      if(data){
        //Splice modifie le tableau et enlever l'élement
        //permet d'afficher de nouveau la page avec tableaux modifer
        const index = this.model.findIndex(m => m.id == id);
        this.model.splice(index, 1);
      }
    }, error => {
      console.log(error);
    }); 
  }

  changeGroup(id:number){
    this.userService.changeGroup(id)
    .subscribe(data => {
      if(data){
        const index = this.model.findIndex(m => m.id == id);
        this.model[index]=data;
      }
    }, error => {
      console.log(error);
    });
  }

}
