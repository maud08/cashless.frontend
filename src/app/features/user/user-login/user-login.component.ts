import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {
  
  //var de travail
  loginFrom: FormGroup;
  
  error_msg = {
    'username': [
      {type: 'required', message: 'Votre Email est requis'},
      {type: 'email', message: 'Votre Email est invalid'},
    ],
    'password': [
      {type: 'required', message: 'Votre mot passe est requis'},
      {type: 'maxLength', message: 'Votre mot passe est incorrect'}
    ]
  }

  constructor(
    private authService: AuthService,
    private session: SessionService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loginFrom = new FormGroup({
      'username': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.max(20),
        Validators.email
      ])),
      'password': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(10)
      ]))
    })
  }
  logged(){
    if(this.loginFrom.invalid) return;
    this.authService.login(this.loginFrom.value)
      .subscribe(
        data =>{
          this.session.start(data);
          this.router.navigateByUrl('/login/profile')
        },
        error => {
          console.log(error);
        }
      );
  }

}
