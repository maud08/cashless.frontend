import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/core/services/user.service';
import { UserModel } from 'src/app/core/models/userModel';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-form-create',
  templateUrl: './user-form-create.component.html',
  styleUrls: ['./user-form-create.component.scss']
})
export class UserFormCreateComponent implements OnInit {

  userCreateForm: FormGroup;
  submitted = false;
  insertValue: UserModel;

  error_msg = {
    'firstName': [
      { type: 'required', message:'Votre prénom est requis'},
      { type: 'max', message:'Votre prénom comporte trop de caratères'}
    ],
    'lastName': [
      { type: 'required', message:'Votre nom est requis'},
      { type: 'max', message:'Votre nom comporte trop de caratères'}
    ],
    'address': [
      { type: 'required', message:'Votre adresse est requise'}
    ],
    'email': [
      { type: 'required', message:'Votre Email est requis'},
      { type: 'email', message:'Votre Email est incorrect'}
    ],
    'password': [
      { type: 'required', message:'Votre mot de passe est requis'},
      { type: 'pattern', message:'Votre mot passe doit contenir minimun 8 caratères, une majuscule, une minuscule, un nombre et un caratère spécial'},
      { type: 'max', message: 'Votre mot passe doit contenir au maximun 20 caractères'}
    ],
    'confirmPassword': [
      { type: "('notSame')", message:'Les mots passe ne sont pas identique '},
    ]
  }

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.userCreateForm = new FormGroup({
      'firstName': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.max(255)
      ])),
      'lastName': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.max(255)
      ])),
      'address' : new FormControl(null, Validators.compose([
        Validators.required
      ])),
      'email' : new FormControl(null, Validators.compose([
        Validators.required,
        Validators.email
      ])),
      'password' : new FormControl(null, Validators.compose([
        Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}'),
        Validators.max(20)
      ])),
      'confirmPassword' : new FormControl(null)
    },{validators: this.checkPassword})
  }
  
  create(){
    this.submitted = true;
    let form = this.userCreateForm.value;
    this.insertValue ={'firstName': form.firstName,'lastName': form.lastName,'address':form.address,'email':form.email,'password': form.password};
    this.userService.insert(this.insertValue)
      .subscribe(data => {
        this.router.navigateByUrl('/login')
      }, error => {
        console.log(error); 
      })
  }

  //création d'un validateur pour comparer les mots passe
  checkPassword(group :FormGroup){
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value;
    return pass === confirmPass ? null : {notSame: true}
  }

 
}


